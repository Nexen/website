-- phpMyAdmin SQL Dump
-- version 4.0.2
-- http://www.phpmyadmin.net
--
-- Počítač: localhost:3306
-- Vygenerováno: Ned 11. srp 2013, 01:29
-- Verze serveru: 5.5.29-0ubuntu0.12.04.1-log
-- Verze PHP: 5.3.10-1ubuntu3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `db32_un_web`
--
CREATE DATABASE IF NOT EXISTS `db32_un_web` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `db32_un_web`;

-- --------------------------------------------------------

--
-- Struktura tabulky `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `category_id` int(255) NOT NULL,
  `category_name` varchar(200) NOT NULL,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Vypisuji data pro tabulku `categories`
--

INSERT INTO `categories` (`id`, `category_id`, `category_name`, `text`) VALUES
(1, 1, 'Stránka', 'testovací stránka'),
(2, 1, 'Stránka', 'testovací stránkaa');

-- --------------------------------------------------------

--
-- Struktura tabulky `en_news`
--

CREATE TABLE IF NOT EXISTS `en_news` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `author` varchar(25) NOT NULL,
  `date` varchar(10) NOT NULL,
  `title` varchar(65) NOT NULL,
  `text` longtext NOT NULL,
  `image` varchar(255) NOT NULL,
  `like` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Vypisuji data pro tabulku `en_news`
--

INSERT INTO `en_news` (`id`, `author`, `date`, `title`, `text`, `image`, `like`) VALUES
(13, 'Nexen', '15.8.2012', 'Official Opening', 'Dear Visitors or Players,<div><br><div>After long days of work, we can introduce our new Server working on patch 3.3.5a Wrath of The Lich King. Try our original Shop and The Best team, what have you ever seen. We have too much edits, that make us better than other normal instant servers! We are trying for best atmosphere and functionality of our server,so if you will see some bug or mistakes, write on our forum and we will repair this things.</div><div>Now is Server&nbsp;<font color="#ff0000">officialy opened</font>.</div><div><br></div><div>Your GM Team</div></div>', '3', 0),
(18, 'Nexen', '24.8.2012', 'Update 1st part', 'First update pack is here!<div><br></div><div>First important thing is our&nbsp;<a href="http://beta2.untold-stories.eu/index.php?act=page&amp;viewid=13" title="" target="_blank">Launcher</a>.&nbsp;Launcher will show you the hot news and other stuff faster than if you will browse on our website. Launcher communicate with our Patch. Launcher can update your Patch in your Data Folder. For your feedback is now able to Delete Patch from our Launcher. Patch dont replace WoW Lore, it will not make you unable to play on other servers. Its not necessary to make copy of WoW.<br></div>', '1', 0),
(22, 'Nexen', '27.8.2012', 'Update 2nd part', 'Greeting to our players,\r\n<br> In this update pack, i will show you some news. All is correctly shown, if you have our Launcher and Patch.<div><div><br></div><div><b>Titles</b></div><div>Titles was created by our players. They are sorted to 5 groups.</div><div><ol><li>Event titles</li><li>Warzone titles</li><li>Class titles</li><li>Cataclysm titles</li><li>Other titles</li></ol><div><b>Transmogrification</b></div></div><div>We have now Transmogrification NPC, which change your display of armor on your character.&nbsp;</div></div>', '1', 0),
(23, 'Nexen', '27.8.2012', 'Launcher Updated', 'Today was Launcher edited for account management and register stuff. Now is important and needed to have Patch for playing on our server. Who will not have our Patch, he can get random WoW Error. So please, use our Patch.<div><br></div><div><b>Main Goals</b></div><div><ul><li>Register stuff in Launcher</li><li>Patch must have everybody, for playing</li></ul><div>Launcher can be downloaded <a href="http://untold-stories.eu/index.php?act=page&amp;viewid=13" title="" target="">here</a>&nbsp;and place it to WoW Folder.</div></div>', '1', 0),
(24, 'Nexen', '27.8.2012', 'New Website', 'Greetings to our Players,<div><br></div><div>Today we loaded up our new website. Hope you like it!</div><div>Some important functions:</div><div><ul><li>Online players</li><li>Profile + profile picture</li><li>List of characters with info</li><li>Unstucker</li><li>Password and Email Reset</li><li>List of Arena Teams</li><li>Server info</li></ul><div>Soon we will upload some new functions.</div></div><div><span style="color: rgb(191, 207, 209); ">Now is Website in betatest, so please be patient if any error occured.</span><br></div>', '2', 0),
(25, 'Nexen', '12.9.2012', 'Teamspeak3 for our players!', '<span style="color: rgb(191, 207, 209); ">Greetings,</span><div>Today you can connect to our Teamspeak server!<br><div>Adress:&nbsp;<b>80.242.34.10:9998</b></div><div>Nick: Your choose, but please, choose i.e. name, which have top played from your account.</div></div>', '3', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `en_pages`
--

CREATE TABLE IF NOT EXISTS `en_pages` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `href_title` varchar(55) NOT NULL,
  `title` varchar(35) NOT NULL,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Vypisuji data pro tabulku `en_pages`
--

INSERT INTO `en_pages` (`id`, `href_title`, `title`, `text`) VALUES
(10, 'Why play there?', 'ProÄ hrÃ¡t na The Untold Stories?', '<p></p><p>We have high quality server with level cap 80.</p><p><span style="color: rgb(191, 207, 209); ">When you create Character, you can buy T8 and S6 armor. You can have 3 professes. Who loves PVP, he can go Arenas or Battlegrounds.&nbsp;</span><span style="color: rgb(191, 207, 209); ">Who loves PVE, he can ho instances or Farm money.</span></p><p>Who loves Both, he can go to our Scripted Warzone. Its the unique of itself and you can find there PVP/PVE. Just check it out!</p><p><span style="color: rgb(191, 207, 209); ">Rates and settings of our Server are set to way, which cant have the whole sets in a day. It needs some time to make some progress. (Its faster than blizzlike :) )</span></p><p><span style="color: rgb(191, 207, 209); ">We have auto event and some other upgrades. Check it out and we promise you, it will not be only wasting your time.</span></p><p></p>'),
(11, 'GM Team', 'VedoucÃ­ team serveru', '<div><br></div>EXON TECHNOLOGIES<div><ul><li>Server Hosting</li><li>Web Hosting</li></ul><div><br></div><div><font color="#ff0000">Administrators:</font></div><div>Nexen</div><div>Vasdev</div><div>Eidam</div><div><ul><li>Server &nbsp;and Database management.</li><li>Full moderation of Forum and making plans for future of server.</li></ul></div><div><br></div><div><font color="#990099">Developers:</font></div><div><span style="color: rgb(191, 207, 209); ">Vasdev</span></div><div>Visky</div><div><ul><li>Core and Database Edit.</li><li>Making Scripts for our server.</li></ul></div><div><br></div><div><font color="#33ffff">Head Gamemaster:</font></div><div>Exitusik</div><div><ul><li>Gamemaster management and control of GM work.</li><li>Help with events and other things.</li><li>Event Reward management.</li></ul></div><div><br></div><div><font color="#00ff00">Gamemaster a Eventer:</font></div><div>Snipex</div><div>Visky</div></div><div><ul><li>Ticket management and help for players.</li><li>Event making and other fun for players.</li></ul></div>'),
(12, 'Support our Server', 'Podpora Serveru', '<div style="text-align: justify;"><span style="color: rgb(191, 207, 209); ">These Banners place on your website or somewhere else and make our community bigger!</span></div><div style="text-align: justify;"><span style="color: rgb(191, 207, 209); "><br></span></div><div style="text-align: justify;"><img src="http://untold-stories.eu/images/banner480x60.gif" alt="" align="none"><span style="color: rgb(191, 207, 209); "><br></span></div><div style="text-align: justify;"><br></div><div style="text-align: justify;"><img src="http://untold-stories.eu/images/banner177x76.png" alt="" align="none"><br></div>'),
(13, 'Launcher', 'Launcher ke staÅ¾enÃ­', '<p>\r\n	Here you can download our Launcher, which is recommended for playing on our server. Please Install it to your WoW Folder.</p>\r\n<p>\r\n	<a href="http://untold-stories.eu/download.php">[TUS Launcher]</a></p>\r\n<p>\r\n	For best running our Launcher, download and install .NET</p>\r\n<p>\r\n	<a href="http://www.slunecnice.cz/sw/microsoft-net-framework/net-framework%204/stahnout/">[.NET Framework]</a></p>'),
(15, 'Connection Guide', '', '<p style="font-size: 14px; color: rgb(213, 213, 213); font-family: Arial, Verdana, sans-serif; ">1. Have installed World of Warcraft: Wrath of the Lich King game</p><div style="color: rgb(213, 213, 213); font-family: Arial, Verdana, sans-serif; font-size: 12px; text-align: justify; ">2. Have Patch 3.3.5a, patches can be downloaded&nbsp;<font color="#0875c2"><span style="font-size: 15px;"><a href="http://patches.cz" title="" target="">here</a></span></font></div><div style="color: rgb(213, 213, 213); font-family: Arial, Verdana, sans-serif; font-size: 12px; text-align: justify; ">(2.1.. Have installed .NET Framework, if you dont have, you can download it&nbsp;<font color="#0875c2"><span style="font-size: 15px;"><a href="http://www.slunecnice.cz/sw/microsoft-net-framework/net-framework%204/stahnout/" title="" target="">here</a></span></font>)</div><div style="color: rgb(213, 213, 213); font-family: Arial, Verdana, sans-serif; font-size: 12px; text-align: justify; ">(2.2. Downloaded our Launcher, register inside ou Launcher, it will automaticaly download our Patch and modify realmlist.wtf)</div><div style="color: rgb(213, 213, 213); font-family: Arial, Verdana, sans-serif; font-size: 12px; text-align: justify; ">3. Register &nbsp;<a href="http://en.untold-stories.eu/index.php?act=register" title="" target=""><font color="#0875c2"><span style="font-size: 15px;">h</span></font>ere</a></div><div style="color: rgb(213, 213, 213); font-family: Arial, Verdana, sans-serif; font-size: 12px; text-align: justify; ">4. Change Realmlist to SET REALMLIST 80.242.34.10:3912</div><div style="color: rgb(213, 213, 213); font-family: Arial, Verdana, sans-serif; font-size: 12px; text-align: justify; ">4.1. Download Patch&nbsp;<font color="#0875c2"><span style="font-size: 15px;"><a href="http://untold-stories.eu/launcherdownload/patch-U.MPQ" title="" target="">here</a></span></font>&nbsp;and place it in i.e.&nbsp;<strong>/WoW/Data/</strong></div><div style="color: rgb(213, 213, 213); font-family: Arial, Verdana, sans-serif; font-size: 12px; text-align: justify; ">5. Have fun on our Server.</div><div style="color: rgb(213, 213, 213); font-family: Arial, Verdana, sans-serif; font-size: 12px; text-align: justify; "><br></div>');

-- --------------------------------------------------------

--
-- Struktura tabulky `main_links`
--

CREATE TABLE IF NOT EXISTS `main_links` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `category_id` int(5) NOT NULL,
  `link` varchar(255) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Vypisuji data pro tabulku `main_links`
--

INSERT INTO `main_links` (`id`, `category_id`, `link`, `category_name`) VALUES
(1, 1, '', 'Domov mládeže'),
(2, 3, '', 'Media'),
(3, 2, '', 'Test'),
(4, 4, '', 'Novinky');

-- --------------------------------------------------------

--
-- Struktura tabulky `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `date` varchar(10) NOT NULL,
  `title` varchar(65) NOT NULL,
  `text` longtext NOT NULL,
  `image` varchar(255) NOT NULL,
  `like` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Vypisuji data pro tabulku `news`
--

INSERT INTO `news` (`id`, `date`, `title`, `text`, `image`, `like`) VALUES
(1, '15.8.2012', 'Welcome', '<p>V&iacute;t&aacute;me V&aacute;s na na&scaron;em serveru. V&scaron;e co jste kdy hledali, požadovali a chtěli, najdete pr&aacute;vě u n&aacute;s! Stač&iacute; si změnit realmlist, zaregistrovat a neru&scaron;eně hr&aacute;t na The Untold Stories! M&aacute;me kvalitn&iacute; z&aacute;zem&iacute; od Teamexon.eu a opravdu hodnotn&eacute; znalosti ve WoW sc&eacute;ně. The Untold Stories je nov&yacute; server a aby mělo smysl hr&aacute;t, pros&iacute;me V&aacute;s o sd&iacute;len&iacute; na&scaron;&iacute; str&aacute;nky (jak webov&eacute; tak FB) aby se lid&eacute; dověděli o nov&eacute;m serveru. Přijďte n&aacute;m uk&aacute;zat co ve V&aacute;s je!</p>\r\n', '1', 460),
(2, '', 'Nábor Devolopera', '<p>Je otevřen n&aacute;bor na Developera, kdo m&aacute; z&aacute;jem napi&scaron;te si přihl&aacute;&scaron;ku na Forum.</p>\r\n\r\n<p><u><strong>Podm&iacute;nky pro přijet&iacute;:</strong></u>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>alespoň 16let</li>\r\n	<li>aktivita minim&aacute;lně 2h denně</li>\r\n	<li>komunikativnost</li>\r\n	<li>Praxe aspoň 2 měs&iacute;ce</li>\r\n</ul>\r\n', '3', 445),
(4, '', 'TeamSpeak', '<p>Zprovoznili jsme pro V&aacute;s Teamspeak.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Adresa:</strong> play.untold-stories.eu:9998</p>\r\n\r\n<p><strong>Nick:</strong> zvolte si takov&yacute;, jak&yacute; m&aacute;te na foru!</p>\r\n\r\n<p>V&aacute;&scaron; GM Team</p>\r\n\r\n<p>&nbsp;</p>\r\n', '1', 408),
(5, '', 'Výzkum', '<p>Pom&aacute;h&aacute;me s v&yacute;zkumem o typech serverů a jejich n&aacute;v&scaron;těvnosti.&nbsp;</p>\r\n\r\n<p>Proto pros&iacute;me o hlasov&aacute;n&iacute; na t&eacute;to adrese&nbsp;<a href="http://hlasov.at/k3wa" target="_blank">http://hlasov.at/k3wa</a></p>\r\n\r\n<p>Děkujeme</p>\r\n', '1', 428),
(7, '', 'Změna realmlistu', '<p>Měnila se IP adresa pro připojen&iacute; na server. Pokud se v&aacute;m nejde připojit, změnte si realmlist na <strong>play.untold-stories.eu:3912</strong>.</p>\r\n\r\n<p>Pokud ani tato adresa nejde, zadejte <strong>set realmlist 80.242.34.198:3912</strong></p>\r\n\r\n<p>Kdyby V&aacute;m to st&aacute;le ne&scaron;lo, napi&scaron;te n&aacute;m :)</p>\r\n', '1', 338),
(9, '', 'VIP spuštěno', '<p>Dnes bylo po dlouh&eacute; době spu&scaron;těno VIP. Prozat&iacute;m je možn&eacute; zas&iacute;lat pouze SMS zpr&aacute;vy. V bliž&scaron;&iacute; budoucnosti budeme pracovat na platbě kartou. Br&aacute;na je aktivov&aacute;na pouze pro Českou republiku. Děkujeme za pochopen&iacute;. VIP naleznete v menu webu po přihl&aacute;&scaron;en&iacute;.</p>\r\n', '3', 293),
(11, '', 'Quest zona', '<p>Na serveru byla otevřena Quest zona, ve kter&eacute; najdete 3 questy a po jejich &uacute;spě&scaron;n&eacute;m dokončen&iacute; na v&aacute;s ček&aacute; hodnotn&aacute; odměna. Dostanete se do n&iacute; pomoc&iacute;&nbsp; teleport&eacute;ra v shopu. Douf&aacute;m, že se v&aacute;m bude nov&aacute; z&oacute;na l&iacute;bit a na chv&iacute;li v&aacute;s zabav&iacute;.</p>\r\n', '1', 310),
(12, '', 'We have to move on!', '<p><span style="line-height: 1.6em;">Jak jste si jistě v&scaron;imli, byla miziv&aacute; aktivita od cel&eacute;ho gm teamu na na&scaron;em serveru. Bylo to z důvodu, že jsme pro v&aacute;s připravili&nbsp;</span><span style="line-height: 1.6em;">zbrusu nov&yacute; server, kter&yacute; je na patch </span><strong style="line-height: 1.6em;">4.3.4</strong><span style="line-height: 1.6em;">. Stalo se tak po zhodnocen&iacute; j&aacute;dra na 4.0.6 patch. J&aacute;dro je naprosto nevyhovuj&iacute;c&iacute; na&nbsp;</span><span style="line-height: 1.6em;">opravy serveru a museli bychom přepisovat opravdu dost věc&iacute;, než bychom se pohli d&aacute;l. Bylo lep&scaron;&iacute; udělat nov&yacute; server na 4.3.4 na </span><span style="line-height: 1.6em;">kter&yacute; je nyn&iacute; v&iacute;ce funkc&iacute; a již jsme na něm pracovali přes dva měs&iacute;ce. V&yacute;sledek je opravdu ohromuj&iacute;c&iacute; v tomto počtu lid&iacute; v teamu.&nbsp;</span><span style="line-height: 1.6em;">Opravili jsme pro V&aacute;s v&iacute;ce než 7</span><strong style="line-height: 1.6em;">0 kouzel</strong><span style="line-height: 1.6em;"> a přes 3</span><strong style="line-height: 1.6em;">0 talentů</strong><span style="line-height: 1.6em;">, kter&eacute; byly naprosto &scaron;patně a nebo nefungovaly vůbec. Nyn&iacute; v&scaron;e běž&iacute; tak&nbsp;</span><span style="line-height: 1.6em;">jak m&aacute; a můžete se tě&scaron;it na dal&scaron;&iacute; &uacute;pravy, kter&eacute; chyst&aacute;me. Na&scaron;im hlavn&iacute;m c&iacute;lem je </span><strong style="line-height: 1.6em;">funkčnost v&scaron;ech spellů a talentů</strong><span style="line-height: 1.6em;">. D&aacute;le battlegroundy,&nbsp;</span><span style="line-height: 1.6em;">raidy, profese, achievementy, a jin&eacute;. Pokud tedy m&aacute;te hru na patchi 4.3.4 nev&aacute;hejte se připojit. Nepotkaj&iacute; V&aacute;s ž&aacute;dn&eacute; chyby, kter&eacute;&nbsp;</span><span style="line-height: 1.6em;">byly na 4.0.6 patchi běžn&eacute;! Realmlist je stejn&yacute; jako nyn&iacute; (set realmlist play.untold-stories.eu:3912)&nbsp;</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>V P&Aacute;TEK 26.4.2013</strong>&nbsp;<span style="color:#FF0000;"><strong>21:00</strong></span> bude nahozen 4.3.4 server.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nem&aacute;te 4.3.4 patch? <a href="http://untold-stories.eu/forum/index.php?topic=119.0">Zde</a> je n&aacute;vod!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://untold-stories.eu/forum/index.php?topic=118.0">Zde</a> je changelog bugů, kter&eacute; jsme opravili, než jsme server spustili.</p>\r\n\r\n<p>Přejeme př&iacute;jemn&eacute; hran&iacute; za svou mocnou postavu</p>\r\n\r\n<p>GM Team</p>\r\n', '3', 197),
(13, '', 'On 4.3.4', '<p>Nyn&iacute; už se můžete připojit. Prozat&iacute;m bych si dovolil nazvat server alphatestem. Je toho sice dost opraveno, ale vždy se něco najde.</p>\r\n\r\n<p>Takže vesele hrajte a berte server nyn&iacute;<span style="line-height: 1.6em;">&nbsp;s rezervou.</span></p>\r\n\r\n<p><strong>Nyn&iacute; již běž&iacute; pro komunitu :)</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nexen</p>\r\n', '1', 246),
(14, '', 'Výpadek', '<p>Dnes (3.6.2013) proběhl update serveru, kter&yacute; skonč&iacute;l n&aacute;sledn&yacute;m v&yacute;padkem.</p>\r\n\r\n<p>V&scaron;e je již opraveno.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Přejeme neru&scaron;en&eacute; hran&iacute; na serveru.</p>\r\n', '3', 141),
(15, '', 'Výpadek', '<p>Zdrav&iacute;m,</p>\r\n\r\n<p>Server se za neoček&aacute;van&eacute;ho času dostal do Loopu. Nyn&iacute; již v&scaron;e zase běž&iacute; jak m&aacute;.</p>\r\n\r\n<p>Přejeme př&iacute;jemn&eacute; hran&iacute;</p>\r\n\r\n<p>Nexen</p>\r\n', '3', 108),
(16, '', 'Nábor do GM Teamu', '<p>Zdrav&iacute;m,</p>\r\n\r\n<p>Již po několik&aacute;t&eacute; se otv&iacute;r&aacute; n&aacute;bor na pozice ingame GM a Developer. Pokud m&aacute;te z&aacute;jem, napi&scaron;te na forum do sekce n&aacute;bor svou přihl&aacute;&scaron;ku.&nbsp;<span style="line-height: 1.6em;">Proběhla čistka cel&eacute;ho teamu a chyb&iacute; aktivita a n&aacute;paditost již neaktivn&iacute;ch GM. Proto pros&iacute;me zvažte, jestli na to opravdu m&aacute;te časově.</span></p>\r\n\r\n<p>Nexen</p>\r\n', '1', 119),
(17, '', 'Oznámení', '<p>V&aacute;žen&iacute; hr&aacute;či,<br />\r\nOznamuji v&aacute;m, že odstupuji z funkce GM.<br />\r\nJe tomu tak z důvodu nefunkčnosti m&eacute;ho PC (grafika a chlazen&iacute;) a nov&yacute; bude za dlouho.<br />\r\nSnad jsem byl ke v&scaron;em spravedliv&yacute; a byl jsem alespoň k něčemu dobr&yacute;.<br />\r\nTřeba se je&scaron;tě někdy uvid&iacute;me (samozřejmě ve hře). :D</p>\r\n\r\n<p><br />\r\nS pozdravem <strong>Olahmate</strong></p>\r\n', '3', 122),
(18, '', 'Next stage', '<p>Zdrav&iacute;m,</p>\r\n\r\n<p>Nast&aacute;v&aacute; nov&aacute; etapa serveru a s n&iacute; přich&aacute;z&iacute; i update serveru z <strong><span style="color:#FF0000;">Alphaverze</span></strong> na <strong><span style="color:#00FF00;">Betaverzi</span></strong>. Byl upraven kompletně svět včetně v&scaron;ech dungeonů, raidů, instanc&iacute; celkově a &uacute;koly. Udělali jsme obrovsk&yacute; kus pr&aacute;ce, av&scaron;ak může se st&aacute;t, že něco bude &scaron;patně či chybět. Proto je třeba ve&scaron;ker&eacute; chyby d&aacute;vat na issue tracker, kter&yacute; je&nbsp;<a href="https://bitbucket.org/Nexen/the-untold-stories/issues/new">zde</a>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Bylo tak&eacute; upraveno j&aacute;dro v cel&eacute;m spektru. V&iacute;ce najdete na changelogu, kter&yacute; se moment&aacute;lně připravuje. Bylo opraveno dal&scaron;&iacute;ch p&aacute;r kouzel a mohlo se st&aacute;t, že na někter&aacute; se zapomnělo. Proto se pros&iacute;m obraťte na issuetracker (link je nad t&iacute;mto odstavcem).</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Server je již online, v&iacute;tejte na betě :)&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>D&iacute;ky že jste s n&aacute;mi a přejeme hezk&yacute; večer.</p>\r\n', '3', 170);

-- --------------------------------------------------------

--
-- Struktura tabulky `novinky`
--

CREATE TABLE IF NOT EXISTS `novinky` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `text` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Vypisuji data pro tabulku `novinky`
--

INSERT INTO `novinky` (`id`, `title`, `author`, `date`, `text`) VALUES
(1, 'Testovací novinka', 'Michal', '19.2.2013', 'text novinky');

-- --------------------------------------------------------

--
-- Struktura tabulky `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `href_title` varchar(55) NOT NULL,
  `title` varchar(35) NOT NULL,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Vypisuji data pro tabulku `pages`
--

INSERT INTO `pages` (`id`, `href_title`, `title`, `text`) VALUES
(10, 'ProÄ hrÃ¡t u nÃ¡s?', 'ProÄ hrÃ¡t na The Untold Stories?', '<p><p>VlastnÃ­me instant server, kde je maximÃ¡lnÃ­ level 80.</p><p><span style="color: rgb(191, 207, 209); ">Ze zaÄÃ¡tku se dÃ¡ koupit T8/s6 a udÄ›lat si profese. Ti co majÃ­ rÃ¡di PVP, tak mohou jÃ­t areny/battleground a ti co PVE funkÄnÃ­ ICC, vÄetnÄ› Lich Kinga.</span><br></p><p><span style="color: rgb(191, 207, 209); ">Ti co majÃ­ rÃ¡di PVP i PVE, mohou jÃ­t do Warzone a ozkouÅ¡et svoje zkuÅ¡enosti s hrÃ¡Äi i potvorami.</span></p><p><span style="color: rgb(191, 207, 209); ">Raty a nastavenÃ­ serveru jsou upraveny tak, aby kaÅ¾dÃ½ nemÄ›l hned celÃ© sety z PVP a nebo PVE. Chce to celkem hodnÄ› Äasu zÃ­skat celÃ½ set (avÅ¡ak ne jako na blizz-liku).</span></p><p><span style="color: rgb(191, 207, 209); ">MÃ¡me rozpis plÃ¡nu, ve kterÃ©m jsou vychytÃ¡vky, co zpÅ™Ã­jemnÃ­ hrÃ¡ÄÅ¯m jak PVP tak PVE. MÃ¡me taktÃ©Å¾ automatizovanÃ© eventy. ZkrÃ¡tka pÅ™ijÄte se podÃ­vat a uvidÃ­te sami. UrÄitÄ› to nebude ztrÃ¡ta Äasu.</span></p></p>'),
(11, 'GM Team', 'VedoucÃ­ team serveru', '<div><br></div>EXON TECHNOLOGIES<div><ul><li>PoskytnutÃ­ Server Hostingu</li><li>PoskytnutÃ­ Web Hostingu</li></ul><div><br></div><div><font color="#ff0000">AdministrÃ¡toÅ™i:</font></div><div>Nexen</div><div>Vasdev</div><div>Eidam</div><div><ul><li>Dohled nad serverem a sprÃ¡va databÃ¡ze a dalÅ¡Ã­ch vÄ›ci.</li><li>PlnÃ¡ moderace fÃ³ra a vytvÃ¡Å™enÃ­ plÃ¡nÅ¯ do budoucna serveru.</li></ul></div><div><br></div><div><font color="#990099">DevelopeÅ™i:</font></div><div>Vasdev</div><div>Visky</div><div><ul><li>Ãšprava jÃ¡dra a databÃ¡ze.</li><li>VÃ½roba scriptÅ¯ a ostatnÃ­ch vÄ›ci tÃ½kajÃ­cÃ­ch se databÃ¡ze nebo jÃ¡dra.</li></ul></div><div><br></div><div><font color="#33ffff">HlavnÃ­ Gamemaster:</font></div><div>Exitusik</div><div><ul><li>Dohled nad vÅ¡emi Gamemastery a Eventery a kontrola jejich prÃ¡ce.</li><li>PÅ™Ã­padnÃ¡ pomoc s eventy a ingame pomocÃ­.</li><li>Dohled nad vyvÃ¡Å¾enostÃ­ odmÄ›n v eventech.</li></ul></div><div><br></div><div><font color="#00ff00">Gamemaster a Eventer:</font></div><div>Snipex</div><div>Visky</div></div><div><ul><li>Å˜eÅ¡enÃ­ TicketÅ¯ a obecnÃ¡ pomoc pro hrÃ¡Äe.</li><li>Tvorba eventÅ¯ a rÅ¯znÃ½ch zÃ¡bav.</li></ul></div>'),
(12, 'PodpoÅ™te nÃ¡Å¡ server', 'Podpora Serveru', '<div style="text-align: justify;"><span style="color: rgb(191, 207, 209); ">Tyto bannery umÃ­stÄ›te na jakÃ©koli webovÃ© strÃ¡nky a tÃ­m zvÃ½Å¡Ã­te naÅ¡Ã­ popularitu.</span></div><div style="text-align: justify;"><span style="color: rgb(191, 207, 209); "><br></span></div><div style="text-align: justify;"><img src="http://untold-stories.eu/images/banner480x60.gif" alt="" align="none"><span style="color: rgb(191, 207, 209); "><br></span></div><div style="text-align: justify;"><br></div><div style="text-align: justify;"><img src="http://untold-stories.eu/images/banner177x76.png" alt="" align="none"><br></div>'),
(13, 'Launcher', 'Launcher ke staÅ¾enÃ­', '<p>\r\n	Zde mÅ¯Å¾ete stÃ¡hnout nÃ¡Å¡ Launcher, kterÃ½ je doporuÄenÃ½ ke hranÃ­ na naÅ¡em serveru. Instalujte do sloÅ¾ky s hrou world of warcraft.</p>\r\n<p>\r\n	<a href="http://untold-stories.eu/TUSLauncher.exe" title="" target="">[TUS Launcher]</a></p>\r\n<p>\r\n	K sprÃ¡vnÃ©mu bÄ›hu aplikace je potÅ™eba .net Framework</p>\r\n<p>\r\n	<a href="http://www.slunecnice.cz/sw/microsoft-net-framework/net-framework%204/stahnout/">[.NET Framework]</a></p>'),
(15, 'Jak se pÅ™ipojit', '', '<p style="font-size: 14px; color: rgb(213, 213, 213); font-family: Arial, Verdana, sans-serif; ">1. MÃ­t nainstalovanou hru World of Warcraft: Wrath of the Lich King</p><div style="color: rgb(213, 213, 213); font-family: Arial, Verdana, sans-serif; font-size: 12px; text-align: justify; ">2. MÃ­t Patch 3.3.5a, potÅ™ebnÃ© patche stÃ¡hnete&nbsp;<font color="#0875c2"><span style="font-size: 15px;"><a href="http://patches.cz" title="" target="_blank">Zde</a></span></font></div><div style="color: rgb(213, 213, 213); font-family: Arial, Verdana, sans-serif; font-size: 12px; text-align: justify; ">(2.1.. MÃ­t nainstalovanÃ½ .NET Framework, pokud ho nemÃ¡te, tak stÃ¡hnete&nbsp;<a href="http://www.slunecnice.cz/sw/microsoft-net-framework/net-framework%204/stahnout/" style="color: rgb(8, 117, 194); font-size: 15px; " title="" target="_blank">Zde</a>)</div><div style="color: rgb(213, 213, 213); font-family: Arial, Verdana, sans-serif; font-size: 12px; text-align: justify; ">(2.2. StaÅ¾enÃ½ nÃ¡Å¡ Launcher, v nÄ›m se zaregistrovat, sÃ¡m zmÄ›nÃ­ realmlist a stÃ¡hne patch)</div><div style="color: rgb(213, 213, 213); font-family: Arial, Verdana, sans-serif; font-size: 12px; text-align: justify; ">3. Zaregistrovat se&nbsp;<a href="http://untold-stories.eu/index.php?act=register" style="color: rgb(8, 117, 194); font-size: 15px; " title="" target="_blank">zde</a></div><div style="color: rgb(213, 213, 213); font-family: Arial, Verdana, sans-serif; font-size: 12px; text-align: justify; ">4. ZmÄ›nit realmlist na SET REALMLIST 80.242.34.10:3912</div><div style="color: rgb(213, 213, 213); font-family: Arial, Verdana, sans-serif; font-size: 12px; text-align: justify; ">4.1. StÃ¡hnout patch&nbsp;<a href="http://untold-stories.eu/launcherdownload/patch-U.MPQ" style="color: rgb(8, 117, 194); font-size: 15px; " title="" target="_blank">zde</a>&nbsp;a umÃ­stit do&nbsp;<strong>/InstalaÄnÃ­_SloÅ¾ka_WoW/Data/</strong></div><div style="color: rgb(213, 213, 213); font-family: Arial, Verdana, sans-serif; font-size: 12px; text-align: justify; ">5. PotÃ© uÅ¾ si uÅ¾Ã­vat naÅ¡eho serveru.</div><div style="color: rgb(213, 213, 213); font-family: Arial, Verdana, sans-serif; font-size: 12px; text-align: justify; "><br></div>');

-- --------------------------------------------------------

--
-- Struktura tabulky `profile_picture`
--

CREATE TABLE IF NOT EXISTS `profile_picture` (
  `acc_id` int(255) NOT NULL,
  `picture` varchar(255) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `profile_picture`
--

INSERT INTO `profile_picture` (`acc_id`, `picture`) VALUES
(366, ''),
(177, ''),
(51, 'http://untold-stories.eu/images/logo.gif'),
(1, 'http://untold-stories.eu/images/profile_pic.jpg'),
(827, '');

-- --------------------------------------------------------

--
-- Struktura tabulky `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Vypisuji data pro tabulku `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'Adresa', 'U Hvízdala 4, 370 11 České Budějovice'),
(2, 'IČO', '600 77 646'),
(5, 'Zřizovatel', 'Jihočeský kraj, U Zimního stadionu 1952/2, 370 76 České Budějovice');

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'Michal', 'f48f601ab4fe46b7a079d1c3bce4163d');

-- --------------------------------------------------------

--
-- Struktura tabulky `vip_payment`
--

CREATE TABLE IF NOT EXISTS `vip_payment` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `timestamp` varchar(100) NOT NULL,
  `phone` int(12) NOT NULL,
  `sms` varchar(75) NOT NULL,
  `shortcode` int(75) NOT NULL,
  `country` varchar(5) NOT NULL,
  `operator` varchar(50) NOT NULL,
  `vip_id` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Vypisuji data pro tabulku `vip_payment`
--

INSERT INTO `vip_payment` (`id`, `timestamp`, `phone`, `sms`, `shortcode`, `country`, `operator`, `vip_id`) VALUES
(18, '2013-03-03T12:49:01', 0, 'PLATBA TUS LIGHT OSMNOTS', 9033330, 'CZ', 'TMOBILE', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
